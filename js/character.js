// feature - add gold / possessions tracker?
// feature - add ability to track temporary effects ... spell with a ceratain game time duration that are up.  Poison etc.
// feature - add a second character - ragnar?
// feature - use mongodb instead of local storage?
// todo - track and show date of last long rest
//todo - make these constants?
//todo - add variable / constant for short-rest and long rest id prefixes?
// todo - when use rod of pact keeper increase spell slots? Or add it to a temporary conditions section?
var character = [];
var shortRestListElName = "SHORT-REST-POWER-LIST";
var shortRestSlotsElName = "SPELL-SLOT-LIST";
var longRestListElName = "LONG-REST-POWER-LIST";
var atWillListElName = "AT-WILL-POWER-LIST";

function loadCharacter() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      // todo - handle case for no local storage?  show warning on page and load basic data.
      // todo - handle case for json request not showing up.   Character data not loaded msg?
      JSON.stringify(this.response);
      character.data = JSON.parse(this.response);
      // todo - double check what I am doing with max spell slots and maxHP do they need to be variables?
      // put data we'll need in local storage if nothing is there now
      if(!localStorage.getItem('currentHp')) {
        localStorage.setItem('currentHp', character.data.hitPoints.maxHp);
        localStorage.setItem('maxHp', character.data.hitPoints.maxHp);
        localStorage.setItem('availableSpellSlots',  character.data.shortRest.spellSlots.available);
        localStorage.setItem('hpLog', '[]');
        localStorage.setItem('usedShortRestPowers', '[]');
        localStorage.setItem('usedLongRestPowers', '[]');
      };
      // display intial data to page
      // character name and description.  Static stuff
      view.displayBaseInfo();
      // current hit points
      view.displayCurrentHp();
      // number of available spell slots
      view.displayAvailableSlots();
      // hit point activity log since last long rest (if any)
      view.displayHpLog();
      // display at will power list
      view.displayPowers(atWillListElName, character.data.atWill.powers);
      //display spell slots
      view.displayPowers(shortRestSlotsElName, character.data.shortRest.spellSlots.spells, "Cast Spell", "SPELL-SLOT-");
      // display short rest powers
      view.displayPowers(shortRestListElName, character.data.shortRest.powers, "Use Power", "SHORT-REST-");
      // determine if short rest powers are available and display the appropriate button
      view.displayUsedPowerButtons('Short Rest');
      // display long rest powers
      view.displayPowers(longRestListElName, character.data.longRest.powers, "Use Power", "LONG-REST-");
      // determin if long rest powers are available and display the appropriate button
      view.displayUsedPowerButtons('Long Rest');
      // set up event listeners
      view.setUpEventListeners();
    } 
  };
  //xhttp.open("GET", "data/sample.json", true);
  xhttp.open("GET", "data/imatra.json", true);
  xhttp.send();
}

var util = {
  arrayToStorage : function (arrayName, arrayToStore){
    var JSONReadyArray = JSON.stringify(arrayToStore);
    localStorage.setItem(arrayName, JSONReadyArray);
  },
  arrayFromStorage: function (arrayName) {
    var JSONArray = localStorage.getItem(arrayName);
    var retrievedArray = JSON.parse(JSONArray);
    return retrievedArray;
  }
};

var character = {
  modifyHp: function(type, changeAmount, date, notes) {
      // todo .. make control for temporary hitpoints 
      // todo .. review D&D rules for unconsious/death..add those states to sheet?  Disable spells when neg hp?
      var currentHp = parseInt(localStorage.getItem('currentHp'));
      var maxHp = parseInt(localStorage.getItem('maxHp'));
      var hpLog = util.arrayFromStorage('hpLog');
      currentHp  = (type === 'Damage'? currentHp - changeAmount : currentHp + changeAmount);
      if (currentHp > maxHp) { /* don't go over maxHP */
        currentHp = maxHp;
      };
      hpLog.push({type:type, amount:changeAmount, notes:notes, date:date, currentHp: currentHp});
      util.arrayToStorage('hpLog', hpLog);
      localStorage.setItem('currentHp', currentHp);
  },
  useSpellSlot: function() {
    var availableSpellSlots = parseInt(localStorage.getItem('availableSpellSlots'));
    if(availableSpellSlots === 0){
      return "error";
    } else {
      availableSpellSlots -- ;
    }
    localStorage.setItem('availableSpellSlots', availableSpellSlots);
  },
  usePower: function(powerType, powerId) {
      var usedPowers = [];
      var powerArrayName = powerType === 'Long Rest' ? 'usedLongRestPowers' : 'usedShortRestPowers';
      usedPowers = util.arrayFromStorage(powerArrayName);
      usedPowers.push(powerId);
      console.log(usedPowers);
      util.arrayToStorage(powerArrayName, usedPowers);
  }
};

var handler = {
  modifyHp: function (e){
    // get data from form
    var parentDiv = e.srcElement.parentElement;
    var inputs = parentDiv.getElementsByTagName('input');
    var type = "Damage";
    var d = new Date();
    var hpChangeRecorded = d.toLocaleTimeString([],{hour: '2-digit', minute: '2-digit'}) + 
      ' on ' + d.toLocaleDateString();
    if (inputs[1].checked === true){
      type = "Healing";
    }
    var changeAmount = Number(inputs[2].value);
    var notes = inputs[3].value;

    //modify hitpoints
    if (changeAmount > 0) {
      character.modifyHp(type, changeAmount, hpChangeRecorded, notes);
    }
    
    //reset form & update viewhp
    inputs[0].checked = true;
    inputs[2].value = '';
    inputs[3].value = '';
    view.displayCurrentHp();
    view.displayHpLog();
  },
  useSpellSlot: function() {
    character.useSpellSlot();
    view.displayAvailableSlots();
  },
  usePower: function(e) {
    var buttonEl = e.srcElement;
    var buttonId = buttonEl.id;
    if (buttonId.startsWith('LONG')) {
      var powerType = 'Long Rest';
    } else{
      var powerType = 'Short Rest';
    }
    character.usePower(powerType, buttonId);
    view.displayUsedPowerButtons(powerType);
  },
  shortRest: function() {
    //restore spell slots
    localStorage.setItem('availableSpellSlots',  character.data.shortRest.spellSlots.available);
    view.displayAvailableSlots();
    localStorage.setItem('usedShortRestPowers', '[]');
    view.displayPowers(shortRestListElName, character.data.shortRest.powers, "Use Power", "SHORT-REST-");
  
  },
  longRest: function() {
    //a  long rest does everything that a short rest does plus restoring hit points and long rest powers
    handler.shortRest();
    //reset hitpoints to full and clear the day's hitpoint activity
    var maxHp = parseInt(localStorage.getItem('maxHp'));
    localStorage.setItem('currentHp', maxHp);
    localStorage.setItem('hpLog', '[]');
    view.displayCurrentHp();
    view.displayHpLog();
    //restore long rest powers
    localStorage.setItem('usedLongRestPowers', '[]');
    view.displayPowers(longRestListElName, character.data.longRest.powers, "Use Power", "LONG-REST-");
  },
  toggleVisible: function(e) {
    var sectionH2El = e.srcElement.parentElement;
    var visiblity = "hide"
    if (e.srcElement.innerHTML === "(show)") {
      visiblity = "show";
    }
    view.setSectionVisibility(sectionH2El, visiblity);
  },
  toggleVisibleAll: function(e) {
    var sectionH2El = e.srcElement.parentElement;
    var sectionButtons = document.getElementsByClassName("vis-toggle");
    var visibility = "hide";
    if (e.srcElement.innerHTML === "show all") {
      visibility = "show";
    }
    for(let i = 0; i < sectionButtons.length; i++) {
      var sectionH2El = sectionButtons[i].parentElement;
      view.setSectionVisibility(sectionH2El, visibility)
    }
    if (visibility === "hide") {
      e.srcElement.innerHTML = "show all";
    } else {
      e.srcElement.innerHTML = "hide all";
    }
  }
};
var view = {
  displayCurrentHp: function() {
    var currentHp = parseInt(localStorage.getItem("currentHp"));
    var currentHpElements = document.querySelector('.current-hp');
    currentHpElements.innerHTML = currentHp;
  },
  displayBaseInfo: function () {
    var characterName = character.data.name;
    var characterDescription = character.data.description;
    var nameElement = document.querySelector('header');
    var fullHitPointElement = document.querySelector('.full-hit-points');
    nameElement.querySelector('h1').innerHTML = characterName;
     nameElement.querySelector('div').innerHTML = characterDescription;
    fullHitPointElement.innerHTML = "Full Hit Points : " + character.data.hitPoints.maxHp;

  },
  displayHpLog: function() {
    //todo - hmm lots of dom manipulation happening here move that stuff to view?
    var hpChanges = util.arrayFromStorage('hpLog');
    var hpLogListElement = document.getElementById('HP-LOG');
    var hpLogListContent = document.createDocumentFragment();
    var mainElement = document.querySelector('main');
    // are there hit point changes to show in a log?
    if (hpChanges.length > 0) {
      // build up list items for the list
      hpChanges.forEach( function(value) {
        var logListItem = document.createElement('li');
        var logListText = value.type + ': ' + value.amount + ' points at ' 
          + value.date + '. Current HP: ' + value.currentHp + '.';
          if (value.notes) {
            logListText +=' Note: ' + value.notes;
          }
        logListItem.appendChild(document.createTextNode(logListText));
        hpLogListContent.appendChild(logListItem);
      });
      // If the section element doesn't exist create one
      if (!hpLogListElement) {
        var hpLogSectionElement = document.createElement('div');
        var hpLogHeadingElement = document.createElement('H2');
        var hpLogSpanElement  = document.createElement('span');
        var hpLogListElement = document.createElement('ul');
        // build up the heading and append to the section
        hpLogHeadingElement.innerHTML = 'Hit Point Log';
        hpLogSpanElement.innerHTML = '(hide)';
        hpLogSpanElement.className = 'vis-toggle';
        hpLogHeadingElement.appendChild(hpLogSpanElement);
        hpLogSectionElement.appendChild(hpLogHeadingElement);
        // build up the list and append to the section
        hpLogListElement.id = 'HP-LOG';
        hpLogListElement.appendChild(hpLogListContent);
        hpLogSectionElement.appendChild(hpLogListElement);
        hitPointSectionElement = mainElement.childNodes[3];
        hitPointSectionElement.appendChild(hpLogSectionElement);
        view.setUpEventListeners();
       } else {
        // clear the list items and add new ones
        hpLogListElement.innerHTML= '';
        hpLogListElement.appendChild(hpLogListContent);
       }
      } else {
        // if the list element exists and there is nothing to populate it with..remove the section from dom
        if(hpLogListElement) {
          // find parent section and remove it
          var parentSection = hpLogListElement.parentElement;
           parentSection.remove();
        }
      }
  },
  displayAvailableSlots: function(availableSpellSlots) {
    var availableSpellSlots = parseInt(localStorage.getItem('availableSpellSlots'));
    var availableSlotsEl = document.getElementById('AVAILABLE-SLOTS');
    availableSlotsEl.innerHTML = availableSpellSlots;
    // if all short rest powers are used and the button isn't already disabled, disable it
    var shortRestSlotsEl = document.getElementById(shortRestSlotsElName);
    if(availableSpellSlots < 1){
        if(!shortRestSlotsEl.classList.contains('disabled')) {
          //todo - also change text of buttons in list to need rest?
          shortRestSlotsEl.className += "disabled";
        }
    }else{
      shortRestSlotsEl.className = "";
    }
  },
  displayUsedPowerButtons: function (powerType) {
      var powerArrayName = powerType === 'Long Rest' ? 'usedLongRestPowers' : 'usedShortRestPowers';
      var usedPowers = util.arrayFromStorage(powerArrayName);
      usedPowers.forEach( function (buttonId, index) {
        var buttonEl = document.getElementById(buttonId);
        buttonEl.className = 'btn disabled';
        buttonEl.innerHTML = 'Need Rest';
      });
  },
  displayPowers: function(powerListElement, powerData, buttonText, idText) {
    // there are plenty of good reasons for templating systems and generally I'd use one
    //but the goal of this project is get back to vanilla js
    powerElement = document.getElementById(powerListElement);
    var listContent = document.createDocumentFragment();
    powerData.forEach(function(power){
      var listItem = document.createElement('li');
      var liButton = document.createElement('span');
      var liDiv = document.createElement('div');
      if(power.name){
        var nameSpan = document.createElement('span');
        nameSpan.appendChild(document.createTextNode(power.name + ' '));
        nameSpan.classList.add('title');
        liDiv.appendChild(nameSpan);
      }
      if(power.notes){
        var notesSpan = document.createElement('span');
        notesSpan.appendChild(document.createTextNode(power.notes + ' '));
        notesSpan.classList.add('notes');
        liDiv.appendChild(notesSpan);
      }
      if(power.reference){
    
      }
      if(power.type){
        var typeSpan = document.createElement('span');
        typeSpan.appendChild(document.createTextNode(power.type + ' '));
        typeSpan.classList.add('type');
        liDiv.appendChild(typeSpan);
      }
      if(buttonText){
        liButton.appendChild(document.createTextNode(buttonText));
        liButton.classList.add("btn");
        if (power.available == false){
          liButton.classList.add("disabled");
        }
        liButton.setAttribute('id', idText + power.id)
        listItem.appendChild(liButton);
      }
      listItem.appendChild(liDiv);
      listContent.appendChild(listItem);
    });
    powerElement.innerHTML = '';
    powerElement.appendChild(listContent);
  },

  setSectionVisibility: function(sectionH2El, visibility) {
    var parentSection = sectionH2El.parentElement;
    var lists = parentSection.getElementsByTagName('ul');
    for (let i =0; i<lists.length; i++){
      if (visibility === "show"){
        lists[i].classList.remove("hidden");
        sectionH2El.querySelector("span").innerHTML = "(hide)";
      }else{
        lists[i].classList.add("hidden");
        sectionH2El.querySelector("span").innerHTML = "(show)";
      }
    }
  },

  setUpEventListeners: function() {
    // event listeners;
    var el = document.getElementById("ADJUST-HP");
    el.addEventListener("click", handler.modifyHp); /* change Hit points */
    el = document.getElementById("LONG-REST"); /* Long Rest Button */
    el.addEventListener("click", handler.longRest)
    el = document.getElementById("SHORT-REST"); /* Short Rest Button */
    el.addEventListener("click", handler.shortRest);
    el = document.getElementById("TOGGLE-VISIBLE-ALL"); /* Hide / Show All Button */
    el.addEventListener("click", handler.toggleVisibleAll);
    el = document.getElementById(shortRestSlotsElName); /* use an available spell slot by casting spell */
    el.addEventListener("click", handler.useSpellSlot);
    el = document.getElementById(shortRestListElName); /* use a short rest power */
    el.addEventListener("click", handler.usePower);
    el = document.getElementById(longRestListElName); /* use a long rest power */
    el.addEventListener("click", handler.usePower);
    var els = document.getElementsByClassName('vis-toggle'); /* Hide or Show a List */
    for(var i=0; i< els.length; i++){
      els[i].addEventListener("click", handler.toggleVisible, false)
    } 
  }
};

(function init(){
    // polyfill for IE11 for String.starsWith
    if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position){
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
  }
  loadCharacter();
}) ();
