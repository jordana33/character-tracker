// todo: host with sample content and provide link
# Dungeons and Dragons Character Tracker:


## Table Contents
1. Overview
2. Technical Considerations
3. Reviewing This Code?  Things to try.
3. Requirements
4. Units of Time
5. Specification
6. Future Development
7. Usability Test Observations

## Overview:

View demo at [http://jordana.riley.name/character-tracker-sample](http://jordana.riley.name/character-tracker-sample).

 I was looking for a fun project with some technical complexity to it, that I couldn't get anywhere else and would use.

I play Dungeons and Dragons maybe 18 times in a year.  My character's abilities shifts from time to time to keep up with current D&amp;D rules.  I don't play often enough that rule changes really sink in, and I tend to forget a bunch of what my character can do.  Also with weeks or months between game sessions I do not always track the state I left my character sheet effectively. What is my character's health?  What powers have I used up?  Am I poisoned or cursed or do I have a protection spell cast on me?

There are some software tools to help create and level up a D&amp;D character, but not much in the way of this kind of tracking type tool.

NOTE: This project was made by me and is intended for personal use only.  If you'd like to do something with it please contact me.

## Technical Considerations:
No frameworks.  No helpers.  No plugins.  My intention with this project is to take a step back and work with plain JavaScript.  I've employed a basic reset.css from Eric Meyer and will probably use a CSS pre-processor at some point. 

## Reviewing this code?  Things to try:
1. Reduce and increase hit points using the form.  Current Hit points will update.  Use the long rest button to reset Hit Points.
2. Cast a spell slot spell and note that total number of spell slots is reduced by one.  Spell slot buttons are not disabled until the number of spell slots equals zero.  Slots are restored by a short or long rest.
3. Use a short or long rest power and note that the button is disabled.  A short or long rest will restore short rest powers.  A long rest power is restored only by a long rest.

## Requirements:
Create a character tracker that will:

- make abilities I have easier to see
- track the state of the character from game session to game session including:
	- hit points
	- spell slots
	- short rest powers
	- long rest powers

## Units of time relevant to this project:
Certain things in this gaming system can be done:

- at will : use as many times as desired.  
- short rest: characters take time out to rest for an hour and get certain powers back.
- long rest :  (characters sleep for 8 hours to recharge hit points and certain powers.

## Specification:
**Hit Points** (health)

- The date of the last long rest should be visible - todo
- Health should be reduced when damage is entered through the form.
- Health should be restored when healing is entered through the form.
- Healing should not increase hit points over max health
- Health should be fully restored with a long rest.
- Changes in Health for a day should be tracked in a log
- Health log should be reset with a long rest.
- It should be possible to add temporary health as needed (Hero's Feast)
- Temporary hit points should be able to raise health past max hit points.
- Temporary hit points should reset with a long rest

**Spell Slots**:
A pool of spells that can be cast at any time if spell slots are available.  The character can cast four different spells from this pool or the same spell 4 times.

- Spell slot spells should be displayed from data with a cast spell button
- The number of available spell slots should be shown and tracked in local storage
- When a spell is used the number of spell slots should be reduced
- When the number of spell slots reaches 0 all spell slot buttons should be disabled, so no more spells can be cast.
- When a short or long rest is taken, spell slots should be restored
- There should be the ability to hide or show the list of spells. The number of slots available should always be visible.


**Short Rest Powers**:
A power a spell or magic item effect that can be used once per short rest time period. 
 
- Short rest powers should be displayed with a cast spell button
- When a power is used, that button should be disabled
- Used powers should be tracked from session to session.
- A short rest or long rest should restore all short rest powers.
- There should be the ability to hide or show the list of powers.

**Long Rest Powers**:
A power or magic item effect that can be used once per long rest time period.

- Long rest powers should be displayed with a cast spell button
- When a power is used, that button should be disabled
- Used powers should be tracked from session to session.
- Only a long rest should restore all short rest powers.
- There should be the ability to hide or show the list of powers.

**Display**:

- **Responsive**  I only expect to use this on an iPhone or iPad.  Main focus is for those devices, but keep it cross browser friendly and looking good on laptop desktop for good form.

 - Phone: show sections in one column
 - iPad and above: show two columns of sections
- **Show/Hide** - can toggle all sections except Hit points and control buttons individually. When all sections are hidden all of the major sections should be visible on the iPhone screen without scrolling.

## Possible future expansion:

- add a way to track time related effects.  (something is in effect that will last for the combat, an hour, a turn etc.)
- allow for this kind of tracking for other members of the group
- provide an executive summary view for game master so he can see where the group's vital statistics at any given time.
- I am organizing powers by how often they can be used but it may also be handy to sort them by the situation I need them in.  (offense, defense, information gathering etc).  Consider adding this to the data structure and have the ability to view abilities in multiple ways

##Usability Test 
Play Test 1 4/12 - Observations:

	- It worked!  I felt I did a better job of looking over all my options.
	- Add quick reference for saves, spell dc, ac ?  Or do I leave this on the character sheet.
	- Healing vs damage buttons should be larger
	- When I click on a spell slot it is a little hard to tell if the click registered.  (Buttons change state with spell slots)
	- I used hide all and show all more often than I thought...sometimes to protect myself from inadvertently pressing a button
	- consider an undo function for spell slots and powers.  Press power button again and get a prompt to undo?  Have option to increase spell slots? (this would both work to undo and take care of power that adds a slot)
	- consider moving buttons to the left in phone mode so that they not likely to be pressed when scrolling with thumb.  
	- consider hide/show for spell details?  might be nice to see all spell quickly and only look at details when I need them.